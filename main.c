/*
 * Arel Jann Clemente | Sort Prime Numbers Using C
 *  
 * This program asks the user to input any amount of integers
 * and outputs sorted prime numbers.
 * 
 * The most complex of this program is converting stored input in
 * char array to an int array. This int array is then sorted and is
 * then checked if each number is a prime number. If number is a
 * prime number, it will be displayed on the output.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_SIZE 100

// I used this guide for qsort(): https://goo.gl/XurwzZ
// this function is needed for qsort()
int cmpfunc (const void * a, const void * b) {
   return ( *(int*)a - *(int*)b );
}
char inArr[MAX_SIZE];
int outArr[MAX_SIZE] = { 0 }; // set all elements to 0 -> check comments above why

int main() {
    register int i;
    bool isPrime(int);
    
    printf("Please enter a list of integers: "); // ask user for input
    fgets(inArr, MAX_SIZE, stdin); // store input as characters in inArr array

    /*
     * if-else clause for input validation
     *  
     * if user inputs nothing, then it will stop the program and return -1
     * if user inputs something, then it will do what it is supposed to do 
     */ 
    if(inArr[0] == '\n') {
        printf("Incorrect or empty input.");
        return -1;
    } else {
        /* 
         * the following solution below can
         * be found here: https://goo.gl/AFrfNi
         * 
         * key functions here are:
         * - strtok(): splits input with a specified delimiter
         *  - strtok() while-loop: splits the whole string
         *  - ptr: pointer is used to assign split string to a variable
         * - atoi(): converts numbers from char to int
         */
        char* ptr = strtok(inArr, " ");
        outArr[0] = atoi(ptr);
        i = 0;
        while(ptr != NULL) {
            outArr[i] = atoi(ptr);
            ptr = strtok(NULL, " ");
            i++;
        }
    }
    // sort numbers in outArr
    qsort(outArr, MAX_SIZE, sizeof(int), cmpfunc);
    // print sorted prime numbers
    printf("Sorted prime numbers from the list: ");
    for(i = 0; i < MAX_SIZE; i++) {
        if(isPrime(outArr[i])) {
            printf("%d ", outArr[i]);
        }
    }
    return 0;
}
// I used this guide for prime number checking: https://goo.gl/DpZcwF
// the solution by Ivan Gritsenko fixed prime checking bugs
bool isPrime(int num) {
    register int i;
    if(num <= 1) return 0;
    for(int i = 2; i * i <= num; i++) {
        if(num % i == 0) return 0;
    }
    return 1;
}
